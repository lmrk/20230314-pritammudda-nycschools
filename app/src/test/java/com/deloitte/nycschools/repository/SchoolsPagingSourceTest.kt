package com.deloitte.nycschools.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PagingSource
import com.deloitte.nycschools.model.School
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolsPagingSourceTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var schoolRepository: SchoolRepository

    private val schoolsPagingSource: SchoolsPagingSource = SchoolsPagingSource()

    companion object {
        val school = School(
            "1234",
            "xyz",
            "Hello this is overview",
            "1234567890",
            "xyz.com",
            "111 xyz",
            "xxx",
            "48083",
            "MI",
        )
        val schools: List<School> = listOf<School>(school, school, school, school, school)
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        startKoin {
            modules(
                module {
                    single { schoolRepository }
                })
        }
    }

    @After
    fun done() {
        stopKoin()
    }

    @Test
    fun pageRefreshSuccess() = runTest {

        given(schoolRepository.getSchools(anyInt(), anyInt())).willReturn(schools)
        val expectedResult = PagingSource.LoadResult.Page(
            data = schools,
            prevKey = null,
            nextKey = 1
        )
        assertEquals(
            expectedResult, schoolsPagingSource.load(
                PagingSource.LoadParams.Refresh(
                    key = 0,
                    loadSize = 5,
                    placeholdersEnabled = false
                )
            )
        )
    }

    @Test
    fun pageLoadHttpError() = runTest {
        val error = RuntimeException("500", Throwable())
        given(schoolRepository.getSchools(anyInt(), anyInt())).willThrow(error)
        val expectedResult = PagingSource.LoadResult.Error<Int, School>(error)
        assertEquals(
            expectedResult, schoolsPagingSource.load(
                PagingSource.LoadParams.Refresh(
                    key = 0,
                    loadSize = 5,
                    placeholdersEnabled = false
                )
            )
        )
    }
}
