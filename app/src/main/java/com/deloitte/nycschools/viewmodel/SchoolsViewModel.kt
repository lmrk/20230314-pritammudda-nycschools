package com.deloitte.nycschools.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.deloitte.nycschools.model.School
import com.deloitte.nycschools.repository.SchoolsPagingSource
import com.deloitte.nycschools.repository.SchoolRepository
import com.deloitte.nycschools.utills.ApiState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SchoolsViewModel() : ViewModel(), KoinComponent {
    val schoolRepository: SchoolRepository by inject()
    val schoolResponse: MutableState<ApiState> = mutableStateOf(ApiState.Empty)
    val satResponse: MutableState<ApiState> = mutableStateOf(ApiState.Empty)

    val schools: Flow<PagingData<School>> = Pager(
        pagingSourceFactory = { SchoolsPagingSource() },
        config = PagingConfig(pageSize = 10)
    ).flow.cachedIn(viewModelScope)

    fun getSingleSchool(dbn: String) {
        viewModelScope.launch {
            schoolRepository.getSingleSchool(dbn).onStart {
                schoolResponse.value = ApiState.Loading
            }.catch {
                schoolResponse.value = ApiState.Failure(it)
            }.collect {
                schoolResponse.value = ApiState.SchoolSuccess(it)
            }
        }
    }

    fun getSATScore(dbn: String) {
        viewModelScope.launch {
            schoolRepository.getSATScore(dbn).onStart {
                satResponse.value = ApiState.Loading
            }.catch {
                satResponse.value = ApiState.Failure(it)
            }.collect {
                satResponse.value = ApiState.SATSuccess(it)
            }
        }
    }
}
