package com.deloitte.nycschools.utills

import com.deloitte.nycschools.model.SATScore
import com.deloitte.nycschools.model.School

sealed class ApiState {
    class SchoolSuccess(val data: List<School>) : ApiState()
    class Failure(val msg: Throwable) : ApiState()
    object Loading : ApiState()
    object Empty : ApiState()
    class SATSuccess(val data: List<SATScore>) : ApiState()
}
