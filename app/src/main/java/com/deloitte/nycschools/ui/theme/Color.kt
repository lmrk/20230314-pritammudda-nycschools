package com.deloitte.nycschools.ui.theme

import androidx.compose.ui.graphics.Color



val Green200= Color(0xFFc6f68d)
val Green500= Color(0xFF75E900)
val Green700= Color(0xFF41c300)
val Teal200 = Color(0xFF03DAC5)