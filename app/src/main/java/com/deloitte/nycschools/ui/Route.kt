package com.deloitte.nycschools.ui

sealed class Route(val route: String) {
    object SchoolList : Route("Schools")
    object SchoolDetails : Route("School Details"){
//        val parameter = "dbn"
//        fun getRoute(dbn: String) = "schools/$dbn"
    }
}
