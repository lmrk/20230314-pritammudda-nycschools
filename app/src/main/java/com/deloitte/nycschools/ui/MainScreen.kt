package com.deloitte.nycschools.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.deloitte.nycschools.R

@Composable
fun MainScreen() {
    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination
    val viewModelStoreOwner = LocalViewModelStoreOwner.current!!

    Scaffold(
        topBar = {
            when (currentDestination?.route) {
                Route.SchoolList.route -> {
                    TopAppBar(
                        title = { Text(stringResource(R.string.school_list)) },
                        backgroundColor = MaterialTheme.colors.primary,
                    )
                }
                Route.SchoolDetails.route -> {
                    TopAppBar(
                        title = { Text(stringResource(R.string.school_details)) },
                        backgroundColor = MaterialTheme.colors.primary,
                        navigationIcon = { UpIcon(navController) }
                    )
                }
            }
        }
    ) { innerPadding ->
        NavHost(
            navController = navController,
            modifier = Modifier.padding(innerPadding),
            startDestination = Route.SchoolList.route
        ) {

            composable(Route.SchoolList.route) {
                CompositionLocalProvider(
                    LocalViewModelStoreOwner provides viewModelStoreOwner
                ) {
                    SchoolsScreen(navController)
                }
            }

            composable(
                route = Route.SchoolDetails.route,
                ) {
                CompositionLocalProvider(
                    LocalViewModelStoreOwner provides viewModelStoreOwner
                ) {
                    SchoolDetailsScreens()
                }
            }
        }
    }
}

@Composable
fun UpIcon(navController: NavController) {
    IconButton(
        onClick = {
            navController.popBackStack()
        }
    ) {
        Icon(
            Icons.Filled.ArrowBack,
            contentDescription = null
        )
    }
}
