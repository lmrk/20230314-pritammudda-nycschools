package com.deloitte.nycschools.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import com.deloitte.nycschools.R
import com.deloitte.nycschools.model.School
import com.deloitte.nycschools.viewmodel.SchoolsViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun SchoolsScreen(navController: NavController) {
    val viewModel = getViewModel<SchoolsViewModel>()
    val schools = viewModel.schools.collectAsLazyPagingItems()

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(
                top = 16.dp, start = 16.dp, end = 16.dp
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        when (val state = schools.loadState.prepend) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                showLoading()
            }
            is LoadState.Error -> {
                showError(message = state.error.message ?: "")
            }
        }
        when (val state = schools.loadState.refresh) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                showLoading()
            }
            is LoadState.Error -> {
                showError(message = state.error.message ?: "")
            }
        }
        items(
            schools.itemCount
        ) {
            SchoolRowItem1(
                school = schools[it],
                viewModel = viewModel,
                navController = navController
            )
        }
        when (val state = schools.loadState.append) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                showLoading()
            }
            is LoadState.Error -> {
                showError(message = state.error.message ?: "")
            }
        }
    }
}

@Composable
fun SchoolRowItem1(school: School?, viewModel: SchoolsViewModel, navController: NavController) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = 8.dp,
                vertical = 8.dp
            )
            .clickable(
                onClick = {
                    if (school != null) {
                        viewModel.getSingleSchool(school.dbn)
                        viewModel.getSATScore(school.dbn)
                    }
                    navController.navigate(Route.SchoolDetails.route)
                }
            ),
        elevation = 4.dp,
        shape = RoundedCornerShape(4.dp)
    ) {
        if (school != null) {
            Text(
                text = school.school_name,
                modifier = Modifier.padding(vertical = 20.dp, horizontal = 10.dp),
                fontWeight = FontWeight.Bold
            )
        }
    }
}

private fun LazyListScope.showLoading() {
    item {
        CircularProgressIndicator(modifier = Modifier.padding(16.dp))
    }
}

private fun LazyListScope.showError(
    message: String
) {
    item {
        Text(
            modifier= Modifier.padding(vertical = 16.dp),
            text = stringResource(id = R.string.no_more_load),
            style = MaterialTheme.typography.h6,
            color = MaterialTheme.colors.error
        )
    }
}
