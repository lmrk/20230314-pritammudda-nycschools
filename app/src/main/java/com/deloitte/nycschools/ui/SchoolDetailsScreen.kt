package com.deloitte.nycschools.ui

import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.deloitte.nycschools.R
import com.deloitte.nycschools.utills.ApiState
import com.deloitte.nycschools.viewmodel.SchoolsViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun SchoolDetailsScreens() {
    val viewModel = getViewModel<SchoolsViewModel>()
    val ctx = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 8.dp, vertical = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(
            modifier = Modifier
                .padding(
                    horizontal = 8.dp,
                    vertical = 4.dp
                ),
            elevation = 4.dp,
            shape = RoundedCornerShape(4.dp)
        ) {
            when (val result = viewModel.schoolResponse.value) {
                is ApiState.Loading -> CircularProgressIndicator()
                is ApiState.SchoolSuccess -> {
                    if (result.data.isNotEmpty()) {
                        LazyColumn(
                            Modifier
                                .padding(
                                    horizontal = 8.dp,
                                    vertical = 8.dp
                                )
                                .fillMaxWidth()
                        ) {
                            items(result.data.size) {
                                Text(
                                    text = result.data[it].school_name,
                                    style = MaterialTheme.typography.h5
                                )
                                Spacer(modifier = Modifier.padding(bottom = 16.dp))

                                Text(
                                    text = result.data[it].overview_paragraph,
                                    style = MaterialTheme.typography.body2
                                )
                                Row(
                                    modifier = Modifier
                                        .padding(
                                            vertical = 4.dp
                                        )
                                ) {
                                    val website = result.data[it].website
                                    Text(
                                        modifier = Modifier.padding(end = 16.dp),
                                        text = stringResource(id = R.string.website),
                                        fontWeight = FontWeight.Bold
                                    )

                                    Text(
                                        modifier = Modifier.clickable {
                                            val urlIntent = Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse(
                                                    "https://www.${
                                                        website.replace(
                                                            "www.",
                                                            ""
                                                        )
                                                    }"
                                                )
                                            )
                                            ctx.startActivity(urlIntent)
                                        },
                                        text = website,
                                        style = MaterialTheme.typography.body2,
                                        color = Color.Blue
                                    )
                                }
                                Row(
                                    modifier = Modifier
                                        .padding(
                                            vertical = 4.dp
                                        )
                                ) {
                                    val address =
                                        "${result.data[it].primary_address_line_1},${result.data[it].city},${result.data[it].state_code} ${result.data[it].zip}"
                                    Text(
                                        modifier = Modifier.padding(end = 16.dp),
                                        text = stringResource(id = R.string.address),
                                        fontWeight = FontWeight.Bold
                                    )
                                    Text(
                                        modifier = Modifier.clickable {
                                            val gmmIntentUri =
                                                Uri.parse("geo:0,0?q=$address")
                                            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                                            mapIntent.setPackage("com.google.android.apps.maps")
                                            ctx.startActivity(mapIntent)
                                        },
                                        text = address,
                                        style = MaterialTheme.typography.body2,
                                        color = Color.Blue
                                    )
                                }
                                Row(
                                    modifier = Modifier
                                        .padding(
                                            vertical = 4.dp
                                        )
                                ) {
                                    val phoneNumber = result.data[it].phone_number
                                    Text(
                                        modifier = Modifier.padding(end = 16.dp),
                                        text = stringResource(id = R.string.phone),
                                        fontWeight = FontWeight.Bold
                                    )
                                    Text(
                                        modifier = Modifier
                                            .padding(top = 2.dp)
                                            .clickable {
                                                val u = Uri.parse("tel:$phoneNumber")
                                                val i = Intent(Intent.ACTION_DIAL, u)
                                                // Launch the Phone app's dialer with a phone
                                                // number to dial a call.
                                                ctx.startActivity(i)
                                            },
                                        text = phoneNumber,
                                        style = MaterialTheme.typography.body2,
                                        color = Color.Blue
                                    )
                                }
                            }
                        }
                    } else {
                        Text(
                            modifier = Modifier
                                .padding(16.dp)
                                .fillMaxWidth(),
                            text = stringResource(id = R.string.no_data_school_detail),
                            style = MaterialTheme.typography.body2
                        )
                    }
                }
                is ApiState.Failure -> {
                    Text(text = stringResource(id = R.string.no_data_school_detail))
                }
                else -> {
                    Log.d("Empty", "Empty")
                }
            }
        }

        Spacer(modifier = Modifier.padding(bottom = 16.dp))

        Card(
            modifier = Modifier
                .padding(
                    horizontal = 8.dp,
                    vertical = 4.dp
                ),
            elevation = 4.dp,
            shape = RoundedCornerShape(4.dp)
        ) {
            when (val result = viewModel.satResponse.value) {
                is ApiState.Loading -> CircularProgressIndicator()
                is ApiState.SATSuccess -> {
                    if (result.data.isNotEmpty()) {
                        LazyColumn(
                            Modifier
                                .padding(
                                    horizontal = 8.dp,
                                    vertical = 8.dp
                                )
                                .fillMaxWidth()
                        ) {
                            items(result.data.size) {
                                Row(
                                    modifier = Modifier
                                        .padding(
                                            vertical = 4.dp
                                        )
                                ) {
                                    Text(
                                        modifier = Modifier.padding(end = 16.dp),
                                        text = "SAT Math Score : ",
                                        fontWeight = FontWeight.Bold
                                    )
                                    Text(
                                        text = result.data[it].sat_math_avg_score,
                                        style = MaterialTheme.typography.body2
                                    )
                                }
                                Row(
                                    modifier = Modifier
                                        .padding(
                                            vertical = 4.dp
                                        )
                                ) {
                                    Text(
                                        modifier = Modifier.padding(end = 16.dp),
                                        text = "SAT Reading Score : ",
                                        fontWeight = FontWeight.Bold
                                    )
                                    Text(
                                        text = result.data[it].sat_critical_reading_avg_score,
                                        style = MaterialTheme.typography.body2
                                    )
                                }
                                Row(
                                    modifier = Modifier
                                        .padding(
                                            vertical = 4.dp
                                        )
                                ) {
                                    Text(
                                        modifier = Modifier.padding(end = 16.dp),
                                        text = "SAT Writing Score : ",
                                        fontWeight = FontWeight.Bold
                                    )
                                    Text(
                                        text = result.data[it].sat_writing_avg_score,
                                        style = MaterialTheme.typography.body2
                                    )
                                }
                            }
                        }
                    } else {
                        Text(
                            modifier = Modifier
                                .padding(16.dp)
                                .fillMaxWidth(),
                            text = stringResource(id = R.string.no_data_sat),
                            style = MaterialTheme.typography.body2
                        )
                    }
                }
                is ApiState.Failure -> {
                    Text(text = stringResource(id = R.string.no_data_sat))
                }

                else -> {
                    Log.d("Empty", "Empty")
                }
            }
        }
    }
}
