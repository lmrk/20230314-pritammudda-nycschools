package com.deloitte.nycschools.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.deloitte.nycschools.model.School
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SchoolsPagingSource() : KoinComponent,PagingSource<Int, School>() {
    private val schoolRepository: SchoolRepository by inject()
        override fun getRefreshKey(state: PagingState<Int, School>): Int? {
            return state.anchorPosition
        }

        override suspend fun load(params: LoadParams<Int>): LoadResult<Int, School> =
            try {
                val from = params.key ?: 0
                val size = params.loadSize
                val data = schoolRepository.getSchools(from = from, size = size)
                if (params.placeholdersEnabled) {
                    val itemsAfter = data.count() - from + data.size
                    LoadResult.Page(
                        data = data,
                        prevKey = if (from == 0) null else from - 1,
                        nextKey = if (data.isEmpty()) null else from + 1,
                        itemsAfter = if (itemsAfter > size) size else itemsAfter,
                        itemsBefore = from
                    )
                } else {
                    LoadResult.Page(
                        data = data,
                        prevKey = if (from == 0) null else from - 1,
                        nextKey = if (data.isEmpty()) null else from + 1
                    )
                }
            } catch (e: Exception) {
                LoadResult.Error(e)
            }
    }
