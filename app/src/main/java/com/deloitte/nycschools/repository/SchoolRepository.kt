package com.deloitte.nycschools.repository

import com.deloitte.nycschools.model.SATScore
import com.deloitte.nycschools.model.School
import com.deloitte.nycschools.network.RetrofitServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SchoolRepository : KoinComponent {
    private val retrofitServices: RetrofitServices by inject()
    private val apiService = retrofitServices.schoolsService

    suspend fun getSchools(from: Int, size: Int):List<School> = apiService.getSchools(size,from)

    fun getSingleSchool(dbn: String): Flow<List<School>> = flow {
        emit(apiService.getSingleSchool(dbn))
    }.flowOn(Dispatchers.IO)

    fun getSATScore(dbn: String): Flow<List<SATScore>> = flow {
        emit(apiService.getSATScore(dbn))
    }.flowOn(Dispatchers.IO)
}
