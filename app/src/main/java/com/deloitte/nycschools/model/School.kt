package com.deloitte.nycschools.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class School(
    val dbn:String,
    val school_name:String,
    val overview_paragraph:String,
    val phone_number:String,
    val website:String,
    val primary_address_line_1:String,
    val city:String,
    val zip:String,
    val state_code:String
)