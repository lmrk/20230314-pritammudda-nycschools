package com.deloitte.nycschools.network

import okhttp3.Interceptor

class HttpEndpoint {
    companion object {
        fun createOkHttpInterceptor(credentialHeaders: Map<String, String> = emptyMap()) =
            Interceptor { chain ->
                chain.request().newBuilder().also { request ->
                    credentialHeaders.forEach { (name, value) ->
                        request.addHeader(name, value)
                    }
                }.build().let { request ->
                    chain.proceed(request)
                }
            }
    }
}