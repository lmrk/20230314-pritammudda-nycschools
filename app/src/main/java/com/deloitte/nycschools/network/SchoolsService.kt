package com.deloitte.nycschools.network

import com.deloitte.nycschools.model.SATScore
import com.deloitte.nycschools.model.School
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsService {
    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"

        //Enhancement- Token Needs to be saved in shared preferences and not here
        fun getCredentials(): Map<String, String> =
            mapOf("X-App-Token" to "OODxS2zaMKLFZaSJRtE7hcu6F")
    }

    @GET("s3k6-pzi2.json")
    suspend fun getSchools(
        @Query("\$limit") limit: Int,
        @Query("\$offset") offset:Int
    ): List<School>

    @GET("s3k6-pzi2.json")
    suspend fun getSingleSchool(
        @Query("dbn") dbn: String
    ): List<School>

    @GET("f9bf-2cp4.json")
    suspend fun getSATScore(
        @Query("dbn") dbn: String
    ): List<SATScore>
}
