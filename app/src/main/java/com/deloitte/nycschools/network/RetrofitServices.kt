package com.deloitte.nycschools.network

import com.deloitte.nycschools.BuildConfig
import com.squareup.moshi.Moshi
import java.util.concurrent.TimeUnit
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.component.KoinComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Implementation for SchoolsService Interface
 */
class RetrofitServices: KoinComponent {

val schoolsService:SchoolsService= createService()

    private fun createService():SchoolsService
    {
        val apiClient = OkHttpClient.Builder().apply {
            // timeouts
            connectTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            readTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            writeTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)

            // content-type header
            addInterceptor(contentTypeUrlInterceptor)

            //app token header
            addInterceptor(HttpEndpoint.createOkHttpInterceptor(SchoolsService.getCredentials()))

            // logging
            addInterceptor(loggingInterceptor)
        }.build()

        // JSON converter
        val moshi: Moshi = Moshi.Builder().build()

        val converterFactory = MoshiConverterFactory.create(moshi)

        val retrofit = Retrofit.Builder().also {
            it.baseUrl(SchoolsService.BASE_URL)
            it.client(apiClient)
            it.addConverterFactory(converterFactory)
        }.build()

        return retrofit.create(SchoolsService::class.java)

    }

    companion object {
        private const val CONNECTION_TIMEOUT_SECONDS: Long = 60
        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val MIME_URL = "application/json"

        private val contentTypeUrlInterceptor = Interceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader(HEADER_CONTENT_TYPE, MIME_URL)
                .build()

            chain.proceed(request)
        }

        private val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = when {
                !BuildConfig.DEBUG -> HttpLoggingInterceptor.Level.NONE
                else -> HttpLoggingInterceptor.Level.BASIC
            }
        }
    }
}
