package com.deloitte.nycschools.di

import com.deloitte.nycschools.repository.SchoolRepository
import org.koin.dsl.module

/**
 * Module to inject repository
 */

val repositoryModule= module{
    single { SchoolRepository() }
}