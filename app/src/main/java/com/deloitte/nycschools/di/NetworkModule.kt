package com.deloitte.nycschools.di

import com.deloitte.nycschools.network.RetrofitServices
import org.koin.dsl.module

/**
 * Module to inject all network services
 */

val networkModule= module{
    single { RetrofitServices() }
}