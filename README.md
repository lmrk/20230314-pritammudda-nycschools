## NYC Schools
This Application provides information on NYC High Schools.

It displays List of schools and on clicking on any school from the list it shows the school details and the SAT scores.

### Architecture

This Application demonstrate following modern Android architectural patterns:

* Fully Kotlin
* MVVM, Repository, ViewModel, and Flow data patterns
* Jetpack Compose for UI
* Dependency injection using Koin
* Retrofit for REST access
* Coroutines for asynchronous tasks
* A standard directory structure
* Junit Tests

